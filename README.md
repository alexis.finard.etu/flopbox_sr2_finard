# FlopBox_SR2_FINARD

## Introduction

Ce projet a pour but de développer une plateforme FlopBox en adoptant le style architectural REST pour nous permettre de centraliser la gestion de nos fichiers distants stockés dans des serveurs FTP tiers.

#### Compilation et exécution


La compilation et l'exécution se font dans le répertoire `flopbox/`.   
La commande `mvn package` permet de compiler le projet.   
La commande `mvn exec:java` permet d'exécuter le projet.  

Une fois le projet en cours d'exécution, il est possible de lancer des commandes curl dans un terminal. Voici quelques exemples de requêtes:

```shell
# Alias:
curl -X GET http://localhost:8080/flopbox/alias #Affiche les couples alias/server , ici pas d'alias
curl -X POST http://localhost:8080/flopbox/alias?"alias=toto&server=unnomquiserachangerplustard" #ajoute un alias toto
curl -X POST http://localhost:8080/flopbox/alias?"alias=totototo&server=ftp.ubuntu.com" #ajoute un alias totototo
curl -X DELETE http://localhost:8080/flopbox/alias?"alias=totototo" #supprime l'alias totototo
curl -X PUT http://localhost:8080/flopbox/alias?"alias=toto&server=ftp.ubuntu.com" #modifie le couple toto
curl -X GET http://localhost:8080/flopbox/alias #Affiche les couples alias/server, ici toto / ftp.ubuntu.com
# Servers:
curl -X GET http://localhost:8080/flopbox/servers #Affiche les alias, ici toto
curl -X GET http://localhost:8080/flopbox/servers/toto # Affiche le contenue du répertoire racine de ftp.ubuntu.com
curl -X GET http://localhost:8080/flopbox/servers/toto?dir="ubuntu-ports/dists" # Affiche le contenue du répertoire ubuntu-ports/dists
curl -X GET http://localhost:8080/flopbox/servers/toto/"ubuntu-ports/dists/bionic/Release.gpg" # Télécharge le fichier Release.gpg là où le projet a été lancé
curl -X GET http://localhost:8080/flopbox/servers/toto/"ubuntu-ports/dists/bionic/Release.gpg"?optdir="/tmp/ok.txt" # Télécharge le fichier dans /tmp/ok.txt


```

#### Démonstration video

![Démonstration vidéo](docs/presentation.gif)

## Architecture

Le projet dispose de deux ressources : alias et serveur.

La ressource alias permet de créer des couples (nom d'alias / url du serveur) que l'on peut modifier voire supprimer.
La ressource serveur permet d'exécuter des requêtes sur un serveur. Par exemple, lister un repertoire, récupérer un fichier ou en télécharger un.

Ces deux ressources se traduisent dans le code par deux classes: Alias et Server.

#### Gestion d'erreur

Dans ce projet, les erreurs attrapées renverront à l'utilisateur un code de retour négatif comme dans ces deux exemples:

```java
try {
    p = Integer.parseInt(port);
}
catch (NumberFormatException  e) {
    return "400 le port donné n'est pas reconnu comme nombre entier\n";
}
```

```java
if (alias == null || server == null || !aliasDict.containsKey(alias)) {
   return "400 impossible de modifier l'alias\n";
}
```

## Code Samples

#### Récupération des erreurs
La fonction connectTo renvoie un type String, ce String vaut null si la connexion s'est bien passé, sinon elle vaut le code d'erreur, par exemple `"400 impossible de se connecter avec les identifiants fournis.\n"`
```java
output = connectTo(ftp, alias, password,user,port);
if (output != null) return output;
```

#### Déconnexion propre

Afin de garantir une déconnexion propre, on ne renvoie pas immédiatement le code d'erreur si la requête ne s'est pas bien passé.
```java
hasbeenstored = ftp.storeFile(src, out);
out.close();
ftp.logout();
ftp.disconnect();
return hasbeenstored ? "200 fichier récupéré\n" : "400 impossible d'obtenir le fichier\n";
```

#### Des classes bien séparées
Le seul moyen pour la classe Server d'avoir accès aux alias est à travers une méthode publique statique de la classe Alias. Cela garantit que la classe Server ne peut modifier la HashMap de la classe Alias 
```java
ftp.connect(Alias.getServerNameFromAlias(alias));
```
#### Une récupération du chemin à l'aide d'un regex
Afin de fournir le chemin du serveur ftp, on passe par un PathParam. Pour cela, un regex est nécessaire.
```java
@GET
@Path("/{alias}/{src: .*}")
@Produces(MediaType.TEXT_PLAIN)	
```

## Information download

La solution afin de bien renvoyer le fichier à l'utilisateur consistait à renvoyer un type Response avec un header spécifié. Malheureusement, notre fonction initiale renvoie un type String ce qui signifie modifier en grande partie le code actuel. La modification n'a donc pas été réalisé.