package ressource;


import java.util.HashMap;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
/**
 *  Représente la ressource alias
 * @author finard
 *
 */
@Path("alias")
public class Alias {
	
	/**
	 * HashMap permettant de stocker les couples alias/serveur
	 */
	static private 	HashMap<String, String> aliasDict = new HashMap<String,String>();
	
	/** 
	 * permet l'affichage des couples alias / serveur
	 * @return String : le code de réponse et les couples
	 */
	@GET
    @Produces(MediaType.TEXT_PLAIN)
	public String getAliasServer() {
		String output = "Nom alias / Nom Serveur:\n";

		for (String alias : aliasDict.keySet()) {
			output = output + alias + " / " + aliasDict.get(alias) +"\n";			
		}
		return "200\n" + output;
	}
	
	/**
	 * ajoute un alias dans la HashMap
	 * @param String : alias l'alias à ajouter
	 * @param String : server le nom du serveur à lier à l'alias
	 * @return String : le code de réponse
	 */
	@POST
    @Produces(MediaType.TEXT_PLAIN)
	public String addAlias(@QueryParam("alias") String alias,@QueryParam("server") String server) {
		if (alias == null || server == null || aliasDict.containsKey(alias)) {
			return "400 impossible d'ajouter l'alias\n";
		}
		aliasDict.put(alias, server);		
		return "200 alias ajouté\n";
	}

	/**
	 * modifie un couple alias/serveur dans la HashMap
	 * @param String : alias l'alias
	 * @param String : server le nouveau serveur associé à l'alias
	 * @return String : le code de réponse
	 */
	@PUT
    @Produces(MediaType.TEXT_PLAIN)
	public String modifyAlias(@QueryParam("alias") String alias,@QueryParam("server") String server) {
		if (alias == null || server == null || !aliasDict.containsKey(alias)) {
			return "400 impossible de modifier l'alias\n";
		}
		aliasDict.remove(alias);
		aliasDict.put(alias, server);		
		return "200 alias modifié\n";
	}

	/**
	 * supprime un alias de la HashMap
	 * @param String : alias l'alias à supprimer
	 * @return String : le code de réponse
	 */
	@DELETE
    @Produces(MediaType.TEXT_PLAIN)
	public String removeAlias(@QueryParam("alias") String alias) {
		if (alias == null || !aliasDict.containsKey(alias)) {
			return "400 impossible de supprimer l'alias\n";
		}
		aliasDict.remove(alias);
		return "200 alias supprimé\n";
	}
	
	/**
	 * renvoie la liste des alias
	 * @return String : la liste des alias
	 */
	public static String getAliasName() {
		String output = "Alias:\n";
		for (String alias : aliasDict.keySet()) {
			output = output + alias+"\n";
		}		
		return output;
	}
	
	/**
	 * renvoie l'url du serveur associé à l'alias
	 * @param String : alias l'alias associé à l'url qui nous interesse 
	 * @return String : l'url du serveur lié à l'alias
	 */
	public static String getServerNameFromAlias(String alias) {
		return aliasDict.get(alias);
	}
}
