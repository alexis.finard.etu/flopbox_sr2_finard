package ressource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
/**
 * Représente la ressource serveur
 * @author finard
 *
 */
@Path("servers")
public class Server {
	
	/**
	 * affiche la liste des alias	
	 * @return String : le code de réponse et la liste des alias
	 */
	@GET
    @Produces(MediaType.TEXT_PLAIN)
	public String getAlias() {
		return "200\n"+Alias.getAliasName();
	}
	
	/**
	 * liste un repertoire sur un serveur ftp
	 * @param String : alias l'alias du serveur ftp que l'on veut acceder
	 * @param String : user le nom d'utilisateur à utiliser pour se connecter sur le serveur ftp
	 * @param String : password le mot de passe à utiliser pour se connecter sur le serveur ftp
	 * @param String : dir le repertoire à lister
	 * @param String : port le numéro de port 
	 * @return String : le code de réponse et le listage du repertoire
	 */
	@GET
	@Path("{alias}")
    @Produces(MediaType.TEXT_PLAIN)
	public String listDir(@PathParam("alias") String alias ,@QueryParam("user") String user, @QueryParam("password") String password,@QueryParam("dir") String dir,@QueryParam("port") String port) {
		String[] listname;
		String output;
		FTPClient ftp = new FTPClient();
		if(user == null) user="anonymous";
		if(password == null) password="anonymous@domain.com";
		
		output = connectTo(ftp, alias, password,user,port);
		if (output != null) return output;
		
		try {
			listname = dir == null ? ftp.listNames() : ftp.listNames(dir) ;
			ftp.logout();
			ftp.disconnect();
		} catch (IOException e) {
			return "400 impossible de trouver le repertoire\n";
		}
		
		if (listname == null) {
			return "400 impossible de lire le repertoire\n";
		}

		output = "Fichier:\n";

		for (String name : listname) {
			output+= name + "\n";
		}

		return "200 repertoire listé\n"+output;
	}
	
	/**
	 * renome un fichier sur un serveur ftp
	 * @param String : alias l'alias du serveur ftp que l'on veut acceder
	 * @param String : src le nom du fichier à renommer
	 * @param String : user le nom d'utilisateur à utiliser pour se connecter sur le serveur ftp
	 * @param String : password le mot de passe à utiliser pour se connecter sur le serveur ftp
	 * @param String : to le nouveau nom du fichier
	 * @param String : port le numéro de port
	 * @return String: le code de réponse
	 */
	@PUT
	@Path("/{alias}/{src: .*}")
    @Produces(MediaType.TEXT_PLAIN)		
	public String RenameFile(@PathParam("alias") String alias ,@PathParam("src") String src,@QueryParam("user") String user, @QueryParam("password") String password,@QueryParam("to") String to,@QueryParam("port") String port) {
		String output;
		FTPClient ftp = new FTPClient();
		if(user == null) user="anonymous";
		if(password == null) password="anonymous@domain.com";
		if(to == null) return "400 QueryParam 'to' non fournie\n";
		output = connectTo(ftp, alias, password,user,port);
		if (output != null) return output;
		try {
			if(!ftp.rename(src,to)) {
				return "400 echec du renommage\n";
			}
			ftp.logout();
			ftp.disconnect();	
		} catch (IOException e) {
			return "400 un problème est survenue lors du renommage de fichier\n";
		}
		return "200 renommage réussie\n";
	}

	/**
	 * récupère un fichier sur un serveur ftp
	 * @param String : alias l'alias du serveur ftp que l'on veut acceder
	 * @param String : src le nom du fichier à récupérer
	 * @param String : user le nom d'utilisateur à utiliser pour se connecter sur le serveur ftp
	 * @param String : password le mot de passe à utiliser pour se connecter sur le serveur ftp
	 * @param String : dir un chemin où stocker le fichier
	 * @param String : port le numéro de port
	 * @return String : le code de réponse
	 */
	@GET
	@Path("/{alias}/{src: .*}")
    @Produces(MediaType.TEXT_PLAIN)	
	public String getFile(@PathParam("alias") String alias,@PathParam("src") String src ,@QueryParam("user") String user, @QueryParam("password") String password,@QueryParam("optdir") String dir,@QueryParam("port") String port) {
		OutputStream out;
		String output;
		FTPClient ftp = new FTPClient();
		if(user == null) user="anonymous";
		if(password == null) password="anonymous@domain.com";
		if(dir == null) {
			String[] tmp;
			tmp = src.split("/");
			dir=tmp[tmp.length-1];
		}

		output = connectTo(ftp, alias, password,user,port);
		if (output != null) return output;
		
		try {
			out = new FileOutputStream(dir);
		} catch (FileNotFoundException e) {
			return "400 impossible de créer un fichier\n";
		}
		
		try {
			boolean hasbeenstored;
			ftp.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
			ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);	
			hasbeenstored = ftp.retrieveFile(src, out);
			out.close();
			ftp.logout();
			ftp.disconnect();
			return hasbeenstored ? "200 fichier récupéré\n" : "400 impossible d'obtenir le fichier\n";

		} catch (IOException e) {
			return "400 un problème est survenue lors de la récupération du fichier\n";
		}
	}
	
	/**
	 * Télécharge un fichier sur un serveur ftp
	 * @param String : alias l'alias du serveur ftp que l'on veut acceder
	 * @param String : src le chemin du fichier à télécharger
	 * @param String : user le nom d'utilisateur à utiliser pour se connecter sur le serveur ftp
	 * @param String : password le mot de passe à utiliser pour se connecter sur le serveur ftp
	 * @param String : dir un chemin où stocker le fichier
	 * @param String : port le numéro de port
	 * @return String : le code de réponse
	 */
	@POST
	@Path("/{alias}/{src: .*}")
    @Produces(MediaType.TEXT_PLAIN)		
	public String putFile(@PathParam("alias") String alias,@PathParam("src") String src ,@QueryParam("user") String user, @QueryParam("password") String password,@QueryParam("optdir") String dir,@QueryParam("port") String port) {
		InputStream out;
		String output;
		FTPClient ftp = new FTPClient();
		if(user == null) user="anonymous";
		if(password == null) password="anonymous@domain.com";
		if(dir == null) {
			String[] tmp;
			tmp = src.split("/");
			dir=tmp[tmp.length-1];
		}

		output = connectTo(ftp, alias, password,user,port);
		if (output != null) return output;
		
		try {
			out = new FileInputStream(dir);
		} catch (FileNotFoundException e) {
			return "400 impossible de créer un fichier\n";
		}
		
		try {
			boolean hasbeenstored;
			ftp.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
			ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);	
			hasbeenstored = ftp.storeFile(src, out);
			out.close();
			ftp.logout();
			ftp.disconnect();
			return hasbeenstored ? "200 fichier récupéré\n" : "400 impossible d'obtenir le fichier\n";

		} catch (IOException e) {
			return "400 un problème est survenue lors de la récupération du fichier\n";
		}
	}
	
	/**
	 * @param String : alias l'alias du serveur ftp que l'on veut acceder
	 * @param String : dir un chemin où créer le répertoire
	 * @param String : user le nom d'utilisateur à utiliser pour se connecter sur le serveur ftp
	 * @param String : password le mot de passe à utiliser pour se connecter sur le serveur ftp
	 * @param String : port le numéro de port
	 * @return String : le code de réponse
	 */
	@POST
	@Path("/{alias}/dir/{dir: .*}")
    @Produces(MediaType.TEXT_PLAIN)		
	public String makeDir(@PathParam("alias") String alias ,@PathParam("dir") String dir ,@QueryParam("user") String user, @QueryParam("password") String password,@QueryParam("port") String port) {
		String output;
		FTPClient ftp = new FTPClient();
		if(user == null) user="anonymous";
		if(password == null) password="anonymous@domain.com";
		output = connectTo(ftp, alias, password,user,port);
		if (output != null) return output;
		try {
			if(!ftp.makeDirectory(dir)) {
				return "400 echec lors de la création du répertoire\n";
			}
			ftp.logout();
			ftp.disconnect();
		} catch (IOException e) {
			return "400 un problème est survenue lors de la création du répertoire\n";
		}
		return "200 création du répertoire réussie\n";
	}
	
	/**
	 * 
	 * @param String : alias l'alias du serveur ftp que l'on veut acceder
	 * @param String : dir le chemin du répertoire à supprimer
	 * @param String : user le nom d'utilisateur à utiliser pour se connecter sur le serveur ftp
	 * @param String : password le mot de passe à utiliser pour se connecter sur le serveur ftp
	 * @param String : port le numéro de port
	 * @return String : le code de réponse
	 */
	@DELETE
	@Path("/{alias}/dir/{dir: .*}")
    @Produces(MediaType.TEXT_PLAIN)		
	public String removeDir(@PathParam("alias") String alias ,@PathParam("dir") String dir ,@QueryParam("user") String user, @QueryParam("password") String password,@QueryParam("port") String port) {
		String output;
		FTPClient ftp = new FTPClient();
		if(user == null) user="anonymous";
		if(password == null) password="anonymous@domain.com";
		output = connectTo(ftp, alias, password,user,port);
		if (output != null) return output;
		try {
			if(!ftp.removeDirectory(dir)) {
				return "400 echec lors de la suppression du répertoire\n";
			}
			ftp.logout();
			ftp.disconnect();
		} catch (IOException e) {
			return "400 un problème est survenue lors de la suppression du répertoire\n";
		}
		return "200 suppression du répertoire réussie\n";
	}

	/**
	 * réalise la connexion sur le serveur ftp
	 * @param FTPClient : ftp le FTPClient où l'on va se connecter
	 * @param String : alias l'alias correspondant au serveur où l'on va se connecter
	 * @param String : password le mot de passe à utiliser pour se connecter sur le serveur ftp
	 * @param String : user le nom d'utilisateur à utiliser pour se connecter sur le serveur ftp
	 * @param String : port le numéro de port
	 * @return String : le code de réponse
	 */
	private static String connectTo(FTPClient ftp, String alias, String password, String user, String port) {		
		boolean connexionfailed;
		try {
			if (port == null) {
				ftp.connect(Alias.getServerNameFromAlias(alias));
			}
			else {
				int p;
				try {
					p = Integer.parseInt(port);
				}
				catch (NumberFormatException  e) {
					return "400 le port donné n'est pas reconnu comme nombre entier\n";
				}
				ftp.connect(Alias.getServerNameFromAlias(alias),p);
			}
			connexionfailed = !FTPReply.isPositiveCompletion(ftp.getReplyCode()); // true si la connexion a échoué
			if (connexionfailed) {
				ftp.disconnect();
				return "400 impossible de se connecter aux serveurs.\n";	
			}
			if(!ftp.login(user, password)) {
		        ftp.disconnect();
				return "400 impossible de se connecter avec les identifiants fournies.\n";
			}
		} catch (IOException e) {
			return "400 problème lors de la connection aux serveur FTP\n";
		}
		return null;
	}
}
	