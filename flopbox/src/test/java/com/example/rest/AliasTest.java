package com.example.rest;

import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ressource.Alias;

import static org.junit.Assert.assertEquals;

public class AliasTest {

    private HttpServer server;

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer();
        // create the client
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    /**
     * Test to see that the message "Got it!" is sent in the response.
     */
    @Test
    public void testgetAliasNameWhenEmpty() {
        assertEquals("Alias:\n", Alias.getAliasName());
    }
}
