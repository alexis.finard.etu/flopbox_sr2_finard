mkdir -p /tmp/toto
rm /tmp/toto/*
rm -f flopbox/Release.gpg
# Alias:
echo curl -X GET http://localhost:8080/flopbox/alias #Affiche les couples alias/server , ici pas d'alias
curl -X GET http://localhost:8080/flopbox/alias
sleep 2
echo curl -X POST http://localhost:8080/flopbox/alias?"alias=toto&server=unnomquiserachangerplustard" #ajoute un alias toto
curl -X POST http://localhost:8080/flopbox/alias?"alias=toto&server=unnomquiserachangerplustard"
sleep 2
echo curl -X POST http://localhost:8080/flopbox/alias?"alias=totototo&server=ftp.ubuntu.com" #ajoute un alias totototo
curl -X POST http://localhost:8080/flopbox/alias?"alias=totototo&server=ftp.ubuntu.com"
sleep 2
echo curl -X DELETE http://localhost:8080/flopbox/alias?"alias=totototo" #supprime l'alias totototo
curl -X DELETE http://localhost:8080/flopbox/alias?"alias=totototo"
sleep 2
echo curl -X PUT http://localhost:8080/flopbox/alias?"alias=toto&server=ftp.ubuntu.com" #modifie le couple toto
curl -X PUT http://localhost:8080/flopbox/alias?"alias=toto&server=ftp.ubuntu.com"
sleep 2
echo curl -X GET http://localhost:8080/flopbox/alias #Affiche les couples alias/server, ici toto / ftp.ubuntu.com
curl -X GET http://localhost:8080/flopbox/alias


# Servers:
sleep 2
echo curl -X GET http://localhost:8080/flopbox/servers #Affiche les alias, ici toto
curl -X GET http://localhost:8080/flopbox/servers
sleep 2
echo curl -X GET http://localhost:8080/flopbox/servers/toto # Affiche le contenue du répertoire racine de ftp.ubuntu.com
curl -X GET http://localhost:8080/flopbox/servers/toto
sleep 2
echo curl -X GET http://localhost:8080/flopbox/servers/toto?dir="ubuntu-ports/dists/bionic" # Affiche le contenue du répertoire ubuntu-ports/dists/bionic
curl -X GET http://localhost:8080/flopbox/servers/toto?dir="ubuntu-ports/dists/bionic"
sleep 2
echo CONTENU DE FLOPBOX AVANT COMMANDE:
ls flopbox
echo curl -X GET http://localhost:8080/flopbox/servers/toto/"ubuntu-ports/dists/bionic/Release.gpg" # Télécharge le fichier Release.gpg là où le projet a été lancé
curl -X GET http://localhost:8080/flopbox/servers/toto/"ubuntu-ports/dists/bionic/Release.gpg"
echo CONTENU DE FLOPBOX APRES COMMANDE:
ls flopbox
sleep 2
echo CONTENU DE TMP AVANT COMMANDE:
ls /tmp/toto
echo curl -X GET http://localhost:8080/flopbox/servers/toto/"ubuntu-ports/dists/bionic/Release.gpg"?optdir="/tmp/toto/ok.txt" # Télécharge le fichier dans /tmp/ok.txt
curl -X GET http://localhost:8080/flopbox/servers/toto/"ubuntu-ports/dists/bionic/Release.gpg"?optdir="/tmp/toto/ok.txt"
echo CONTENU DE TMP APRES COMMANDE:
ls /tmp/toto

echo CONTENU DE /tmp/toto:
cat /tmp/toto/ok.txt
